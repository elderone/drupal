#!/bin/bash

set -x

## vars
branches=$(cat /srv/www/roles | cut -d':' -f2 | sort -u)

## functions
set_up_db_settings() {
    db_pass=$(grep ${branch} /srv/www/${database}_settings | cut -d'=' -f2)
    db_user=$(grep ${branch} /srv/www/${database}_users | cut -d'=' -f2)
    db_name=$(grep ${branch} /srv/www/${database}_names | cut -d'=' -f2)

    echo -e "db_name=${db_name}\ndb_user=${db_user}\ndb_pass=${db_pass}\ndb_host=db\nmemcached_host:port=memcached:11211" > ${SOURCE_PATH}/${branch}/${database}_settings
}
get_sources() {
    if [[ ${branch} = srv ]]; then
	su - www-data -c "git clone -b master ${GIT_STRING} ${SOURCE_PATH}/${branch}/webdata"
    else
	su - www-data -c "git clone -b ${branch} ${GIT_STRING} ${SOURCE_PATH}/${branch}/webdata"
    fi
}

## set permissions
chown www-data:www-data -R /srv/www

## database type
if [[ -f /srv/www/mysql_settings ]]; then database=mysql;
elif [[ -f /srv/www/postgresql_settings ]]; then database=postgresql;
fi

## set up users
for users in $(cat /srv/www/roles); do
    user=${users%:*}
    branch=$(echo ${users} | cut -d':' -f2)
    if [[ ${branch} = master ]]; then
	mkdir -p ${SOURCE_PATH}/.ssh
	cat /srv/www/user_keys/${user} >> ${SOURCE_PATH}/.ssh/authorized_keys
	chmod 600 ${SOURCE_PATH}/.ssh/authorized_keys
	useradd -d ${SOURCE_PATH} -u $(id -u www-data) -s /bin/bash -U -o ${user}
	cp -f /etc/skel/{.bash_logout,.bashrc,.profile} ${SOURCE_PATH}/
    else
	mkdir -p ${SOURCE_PATH}/${branch}/.ssh
	cat /srv/www/user_keys/${user} >> ${SOURCE_PATH}/${branch}/.ssh/authorized_keys
	chmod 600 ${SOURCE_PATH}/${branch}/.ssh/authorized_keys
	useradd -d ${SOURCE_PATH}/${branch} -u $(id -u www-data) -s /bin/bash -U -o ${user}
	cp -f /etc/skel/{.bash_logout,.bashrc,.profile} ${SOURCE_PATH}/${branch}/
    fi
done

chown www-data:www-data -R ${SOURCE_PATH}
chown -R www-data:www-data /usr/share/php/drush/lib

for branch in ${branches}; do
    if [[ ${branch} = master ]]; then branch=srv; fi
## get sources
    get_sources
## set up databases
    set_up_db_settings
## set up nginx
    site=${PROJECT}.${branch}
    cp -f /etc/nginx/sites-available/example.com.conf /etc/nginx/sites-enabled/${site}.conf
    sed -i -e "s@root /var/www/sites/example.com;@root ${SOURCE_PATH}/${branch}/webdata;@" /etc/nginx/sites-enabled/${site}.conf
    if [[ ${branch} = srv ]]; then
	sed -i -e "s/server_name www./server_name www.${PROJECT}.newpage.by www./" /etc/nginx/sites-enabled/${site}.conf
	sed -i -e "s/server_name ex/server_name ${PROJECT}.newpage.by ex/" /etc/nginx/sites-enabled/${site}.conf
	sed -i -e "s@://example.com@://${PROJECT}.newpage.by@g" /etc/nginx/sites-enabled/${site}.conf
    fi
    sed -i -e "s@example.com@${site}@g" /etc/nginx/sites-enabled/${site}.conf
    sed -i -e "s@listen \[@#listen \[@g" /etc/nginx/sites-enabled/${site}.conf
## remove ssl section
    if [[ ${SSL} = '' ]]; then sed -i -e '/} # HTTP server/q' /etc/nginx/sites-enabled/${site}.conf; fi
done
