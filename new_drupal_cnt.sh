#!/bin/bash

set -x

### constants
INSTALL=${INSTALL}
GIT_STRING=${GIT_STRING}
PROJECT=${PROJECT}
PROJECT_PATH=${PROJECT_PATH}
SOURCE_PATH=${SOURCE_PATH}
DATABASE=${DATABASE}

### functions
help() { echo "Usage: $0 \$git_string [mysql|postgresql] [new]"; }
set_up_mysql() {
    db_name=$(echo ${PROJECT} | tr - _)_${branch}
    db_user="'u${SSH_PORT}_${branch}'"
    db_pass="'$(pwgen -s1 12)'"

    echo -e "${branch}=${db_pass}" >> ${PROJECT_PATH}/${DATABASE}_settings
    echo -e "${branch}=${db_user}" >> ${PROJECT_PATH}/${DATABASE}_users
    echo -e "${branch}=${db_name}" >> ${PROJECT_PATH}/${DATABASE}_names

    mysql -e "drop database if exists ${db_name};"
    mysql -e "drop user ${db_user};"
    mysql -e "create user ${db_user} identified by ${db_pass}; \
    create database ${db_name}; \
    grant all privileges on ${db_name}.* to ${db_user};"
}
set_up_postgresql() {
    db_name=$(echo ${PROJECT} | tr - _)_${branch}
    db_user="${PROJECT}_${branch}"
    db_pass="'$(pwgen -s1 12)'"

    echo -e "${branch}=${db_pass}" >> ${PROJECT_PATH}/${DATABASE}_settings
    echo -e "${branch}=${db_user}" >> ${PROJECT_PATH}/${DATABASE}_users
    echo -e "${branch}=${db_name}" >> ${PROJECT_PATH}/${DATABASE}_names

    psql -U docker -h 172.16.1.101 -c "drop database if exists ${db_name};"
    psql -U docker -h 172.16.1.101 -c "drop user if exists ${db_user};"
    psql -U docker -h 172.16.1.101 -c "CREATE USER ${db_user} WITH password ${db_pass};"
    psql -U docker -h 172.16.1.101 -c "CREATE DATABASE ${db_name};"
    psql -U docker -h 172.16.1.101 -c "GRANT ALL privileges ON DATABASE ${db_name} TO ${db_user};"
}

### test arguments
for arg in $*; do
    if  [[ $(echo ${arg} | grep -c 'git@') != 0 ]]; then GIT_STRING=${arg}; PROJECT=$(echo $1 | cut -d'/' -f2 | cut -d'.' -f1); PROJECT_PATH=/dock/drupal/${PROJECT}; SOURCE_PATH=${PROJECT_PATH}/${PROJECT};
    elif [[ ${arg} = new ]]; then INSTALL=new;
    elif [[ ${arg} = mysql ]]; then DATABASE=mysql; rm -f ${PROJECT_PATH}/${DATABASE}_settings;
    elif [[ ${arg} = postgresql ]]; then DATABASE=postgresql; rm -f ${PROJECT_PATH}/${DATABASE}_settings;
    fi
done
if [[ ${GIT_STRING} = '' ]]; then help; exit 1; fi
if [[ ${DATABASE} = '' ]]; then help; exit 1; fi
#if [[ ${DATABASE} != '' && ${INSTALL} != new ]]; then help; exit 1; fi
if [[ ${INSTALL} != new && ! -d ${PROJECT_PATH} ]]; then help; exit 1; fi

if [[ ${DATABASE} = mysql ]]; then db_cont=mariadb;
elif [[ ${DATABASE} = postgresql ]]; then db_cont=postgres;
fi

### remove old container if exists
if [[ $(docker ps -a | grep -c ${PROJECT}) != 0 ]]; then
    WEB_PORT=$(docker inspect ${PROJECT} | grep HostPort | awk '{print $2}' | tr -d \" | sort -u | grep 28)
    SSH_PORT=$(docker inspect ${PROJECT} | grep HostPort | awk '{print $2}' | tr -d \" | sort -u | grep 22)
#    semanage port -d -p tcp ${WEB_PORT}
#    semanage port -d -p tcp ${SSH_PORT}
    docker stop ${PROJECT} && docker rm -f ${PROJECT}
fi

wait $!

### create environment settings file
if [[ ${INSTALL} = new ]]; then 
    rm -rf ${PROJECT_PATH}
    mkdir -p ${SOURCE_PATH}
    touch ${PROJECT_PATH}/firstrun
    cp -rf ./install/* ${PROJECT_PATH}/
    echo -e "GIT_STRING=${GIT_STRING}\nPROJECT=${PROJECT}\nSOURCE_PATH=/srv/www/${PROJECT}" > ${PROJECT_PATH}/settings
fi


### check and set ports
for port in {001..099}; do
    if [[ $(netstat -nlt | grep -c 28${port}) > 0 ]]; then
	continue
    else 
	WEB_PORT=28${port}
	SSH_PORT=22${port}
	break
    fi
done

for branch in $(cat ${PROJECT_PATH}/roles | cut -d':' -f2 | sort -u); do
    if [[ ${branch} = master ]]; then branch=srv; fi
    ### set up database
    if [[ ${INSTALL} = new ]]; then
	if [[ ${DATABASE} = mysql ]]; then set_up_mysql;
	elif [[ ${DATABASE} = postgresql ]]; then set_up_postgresql;
	fi
    fi
    ### set up nginx
    site=${PROJECT}.${branch}
    rm -f /etc/nginx/conf.d/${site}.conf
    cp -f nginx_drupal_dummy.conf /etc/nginx/conf.d/${site}.conf
    sed -i -e "s/drupal_port/${WEB_PORT}/g" /etc/nginx/conf.d/${site}.conf
    sed -i -e "s/example.com/${site}/g" /etc/nginx/conf.d/${site}.conf
    if [[ ${branch} = srv ]]; then
	sed -i -e "s/server_name/server_name ${PROJECT}.newpage.by/" /etc/nginx/conf.d/${site}.conf
    fi
done

### reload nginx
systemctl reload  nginx.service

### run new container
docker run -d \
  --privileged \
  --link=${db_cont}:${DATABASE} \
  --link=memcached:memcached \
  --name=${PROJECT} \
  -p 127.0.0.1:${WEB_PORT}:80 \
  -p 172.16.1.101:${SSH_PORT}:22 \
  -v ${PROJECT_PATH}:/srv/www \
  -v /etc/localtime:/etc/localtime:ro \
  -v ${PROJECT_PATH}/nginx:/etc/nginx/sites-enabled \
  -h ${PROJECT}.newpage.by \
  --env-file=${PROJECT_PATH}/settings \
  v_sage/drupal:0.1

### create startup script for systemd
cat > /lib/systemd/system/${PROJECT}.service << EOF
[Unit]
Description=${PROJECT} container
After=docker.service

[Service]
Restart=always
ExecStart=/usr/bin/docker start -a ${PROJECT}
ExecStop=/usr/bin/docker stop -t 2 ${PROJECT}

[Install]
WantedBy=multi-user.target
EOF
systemctl enable ${PROJECT}.service

### selinux and firewalld permissions
semanage port -a -p tcp -t http_port_t ${WEB_PORT}
semanage port -a -p tcp -t ssh_port_t ${SSH_PORT}
firewall-cmd --permanent --add-port ${WEB_PORT}/tcp
firewall-cmd --permanent --add-port ${SSH_PORT}/tcp

### start systemd container
systemctl start ${PROJECT}.service
