#!/bin/sh

env | grep _PORT >> /etc/environment

if [ -e /srv/www/firstrun ]; then /srv/www/install.sh && rm -f /srv/www/firstrun; fi

/usr/bin/supervisord -n
