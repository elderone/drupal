#!/bin/bash

docker run -d \
 --privileged \
 --name mariadb \
 -p 3306:3306 \
 -v /srv/docker/mariadb/data:/data \
 -e USER="root" -e PASS="PaSsWoRd" \
 paintedfox/mariadb

cat > /lib/systemd/system/mariadb.service << EOF
[Unit]
Description=mariadb container
Author=Me
After=docker.service

[Service]
Restart=always
ExecStart=/usr/bin/docker start -a mariadb
ExecStop=/usr/bin/docker stop -t 2 mariadb

[Install]
WantedBy=multi-user.target
EOF

systemctl enable mariadb.service
systemctl start mariadb.service
firewall-cmd --permanent --add-service mysql
