#!/bin/bash

docker run -d \
 --name memcached \
 -p 11211:11211 \
 -e MAX_MEM=256
 sylvainlasnier/ubuntu

cat > /lib/systemd/system/memcached.service << EOF
[Unit]
Description=memcached container
Author=Me
After=docker.service

[Service]
Restart=always
ExecStart=/usr/bin/docker start -a memcached
ExecStop=/usr/bin/docker stop -t 2 memcached

[Install]
WantedBy=multi-user.target
EOF

systemctl enable memcached.service
systemctl start memcached.service
firewall-cmd --permanent --add-port 11211/tcp
